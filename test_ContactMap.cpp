#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <sstream>

#include "skutil.h"
#include "contactmap.h"

using namespace std;



int main(int argc, char* argv[])
{

//   {
//     cout << "TESTING Creation and Iteration of ContactMap::ContactData\n";
//     
//     const int minCutoff = 3;
//     const int maxCutoff = 20;
//     ContactMap::ContactData* data = new ContactMap::ContactData;
//     (*data)[string("bla")] = Contact(minCutoff,maxCutoff);
//     
//     for(ContactMap::ContactData::iterator iter = (*data).begin(); iter != (*data).end(); ++iter)
//     {
//       string              key = iter->first;
//       Contact val = iter->second;
//       
//       cout << "Key: " << key << "\n"; // DEBUG: Segmentation Fault !?
//       cout << "Value (counts): \n";
//   //     out << " : ";
//       for(int j=0; j<(signed)val.size(); j++)
//       {
//         cout << "\t" << j+minCutoff << ", " << val[j] << "\n";
//       }
//       cout << "\n";
//     }
//     
//     delete data;
//   }
  
    stringstream ss1, ss2;
    vector<ContactMap> contacts;
    
//     cout << "TESTING Creation and print() of ContactMap\n";
    
    ContactMap cmap;
    
//     cout << "Defined cmap.\nNow initializing contacts.\n";
    
    cmap.initialiseContacts(string("foo"));
    cmap.initialiseContacts(string("bar"));
    cmap["bla"].fill(1.0);
    cmap["foo"].fill(1.5);
    cmap["bar"].fill(2.5);
    cmap[string("bla")][cmap.getMinCutoff()] += 1.0;
    cmap[string("foo")][cmap.getMinCutoff()] += 1.0;
    cmap.add(string("bar"), cmap.getMinCutoff(), 1.0);
    
    contacts.push_back(cmap); // this makes a copy
    
//     cout << "Done initializing contacts.\nNow printing.\n";
    
    require(cmap.hasContacts("bla"), "Cmap has no key 'bla'");
    require(cmap.hasContacts("foo"), "Cmap has no key 'foo'");
    require(cmap.hasContacts("bar"), "Cmap has no key 'bar'");
    
    cmap.print(ss1);
    
//     cout << "Getting Contact for 'bla'.\n";
//     
//     Contact c = cmap[string("bla")];
//     
//     cout << "Printing Contact for 'bla':\n";
//     c.print();
    
    
    cout << "Creating second ContactMap and add()ing content of first.\n";
    ContactMap cmap2;
    cmap2.add(cmap);
    contacts.push_back(cmap2);
    
    cmap2.print(ss2);
    
    require(ss1.str() == ss2.str(), "Adding cmap1 to an empty cmap2 did not yield the same as cmap1");
    
    
    cout << "add()ing content of first once more.\n";
    cmap2.add(cmap);
    contacts.push_back(cmap2);
    
    ss2.str("");
    ss2.clear();
    require(ss2.str() == "");
    cmap2.print(ss2);
    
    require(ss1.str() != ss2.str(), "Adding cmap1 to cmap2 (which is the same as cmap1) did not change cmap2");
    
    {
      stringstream ss3;
      contacts[0].print(ss3);
      require(ss1.str() == ss3.str(), "contacts[0] cmap is wrong");
    }
    
    {
      stringstream ss3;
      contacts[1].print(ss3);
      require(ss1.str() == ss3.str(), "contacts[1] cmap is wrong");
    }

    {
      stringstream ss3;
      contacts[2].print(ss3);
      require(ss2.str() == ss3.str(), "contacts[2] cmap is wrong");
    }

//     cout << "\n\n\nPrinting vector of ContactMaps....\n";
//     for(vector<ContactMap>::iterator it=contacts.begin(); it<contacts.end(); it++)
//     {
//       it->print();
//       vector<double> colsums = it->getColumnSums();
//       cout << "Column Sums:\n" << toString(colsums) << "\n";
//       cout << "Total Sum:\n" << it->getTotalSum() << "\n\n";
//     }
    
    {
      ofstream outf("test_ContactMap2.out");
      serializeContactMap(cmap2, outf);
      outf.close();
    }
    
    {
      ContactMap cmap3 = unserializeContactMap("test_ContactMap2.out");
      stringstream ss3;
      cmap3.print(ss3);
      require(ss2.str() == ss3.str(), "(un)serialisation not working");
    }
    
    {
      ContactMap::Iterator cmap2_it = cmap2.getIterator();
      cout << "NOW PRINTING USING CMAP ITERATOR!\n";
      while(cmap2_it.next())
      {
        cout << cmap2_it.getKey() << " : ";
        cmap2_it.getContact().print();
        cout << "\n";
      }
      cout << "done\n";
    }
    
//     {
//       ContactMap::const_iterator it = cmap2.begin();
//       
//       for(; it != cmap2.end(); it++)
//       {
//         cout << it->first << " : ";
//         it->second.print();
//         cout << "\n";
//       }
//     }
    
    {
      cout << "Now copying cmap using ContactMapIterator\n";
      stringstream ss3;
      ContactMap::Iterator cmap2_it = cmap2.getIterator();
      ContactMap cmap3(cmap2_it);
      cmap3.print(ss3);
      require(ss2.str() == ss3.str(), "copying cmap via a ContactMapIterator not working");
      cout << "done\n";
    }
}
