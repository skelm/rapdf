/*
 *  contactmap.h
 *
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#ifndef CONTACTMAP_H
#define CONTACTMAP_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <vector>

#include "skutil.h"


class Contact
{
  private:
    std::vector<double> data;
    int imin;
    int imax;
  
  public:
    typedef std::vector<double>::iterator iterator;
    typedef std::vector<double>::const_iterator const_iterator;
    
    Contact(int imn=0, int imx=20, double defValue=0.0);
    
    iterator begin() {return data.begin();}
    const_iterator begin() const {return data.begin();}
    
    iterator end() {return data.end();}
    const_iterator end() const {return data.end();}
    
    unsigned int size() const { return data.size(); }
    
    const std::vector<double>& asVector() const {return data;}
    
    int getIMin(){return imin;}
    int getIMax(){return imax;}
    void fill(double value);
    void add(Contact& other, double factor=1.0);
    
//     Contact& operator=(const Contact& right)
//     {
//       // Handle self-assignment:
//       if(this == &right) return *this;
//       data = right.data;
//       imin = right.imin;
//       imax = right.imax;
//       return *this;
//     }
    
    double& operator[](int i);
    double& operator[](double i);
    const double& operator[](int i) const;
    const double& operator[](double i) const;
    
    void print(std::ostream& out = std::cout) const;
};


inline double sum(const Contact& c)
{
  return sum(c.asVector());
}
inline std::string toString(const Contact& c)
{
  return toString(c.asVector());
}



class ContactMapIterator
{
  public:
    virtual ~ContactMapIterator(){}
    
    virtual int getIMin() const = 0;
    virtual int getIMax() const = 0;
//     virtual double getFactor() const = 0;
//     virtual double getDefaultValue() const = 0;
    virtual bool next() = 0;
    virtual std::string getKey() const = 0;
    virtual Contact getContact() const = 0;
    virtual void reset() = 0;
};



class ContactMapReader : public ContactMapIterator
{
  private:
    const char* fname;
    std::ifstream fin;
    std::streampos startpos;
    
    int imin;
    int imax;
//     double factor;
//     double defaultValue;
    
    std::string key;
    Contact c;
    
    void init();
  
  public:
    ContactMapReader(const char* filename);
    
    ContactMapReader(const ContactMapReader& cmr);
    
    int getIMin() const { return imin; }
    int getIMax() const { return imax; }
//     double getFactor() const { return factor; }
//     double getDefaultValue() const {return defaultValue;}
    
    bool next();
    
    std::string getKey() const;
    
    Contact getContact() const;
    
    void reset();
    
    void close();
};


class ContactMap
{
  private:
    typedef std::map<const std::string, Contact> ContactData;
    ContactData data;
    int minCutoff;
    int maxCutoff;
//     double factor;
    std::vector<double> columnSums;
    bool redoSums;
//     double defaultValue;
    //const static double defaultValue = 0.0;
  
  public:
    typedef ContactData::iterator iterator;
    typedef ContactData::const_iterator const_iterator;
    
    ContactMap(int imin=3, int imax=20, double fact=1.0, double defVal=0.0);
    ContactMap(const ContactMap& c);
    ContactMap(ContactMapIterator& cread);
    
    unsigned int size() const {return data.size();}
    bool isEmpty() const {return 0 == data.size();}
    
    bool hasContacts(std::string key) const;
    
    Contact& operator[](std::string key);
    const Contact& operator[](std::string key) const;
    
    double getTotalSum();
    
    std::vector<double> getColumnSums();
    
    void clearSums() {redoSums = true;}
    
    Contact& initialiseContacts(const std::string key);

    void add(const std::string key, double dist, double increment=1.0);
    void add(const std::string key, Contact contact, double factor=1.0);
    void add(ContactMap& contacts, double factor=1.0);
        
    void print(std::ostream& out = std::cout) const;
    
    int getMinCutoff() const {return minCutoff;}
    int getMaxCutoff() const {return maxCutoff;}
    int getIMin() const {return minCutoff;}
    int getIMax() const {return maxCutoff;}
//     double getFactor() const {return factor;}
//     double getDefaultValue() const {return defaultValue;}
    
//     void setFactor(double f) {factor = f;}
    
    iterator begin() {return data.begin();}
    iterator end()   {return data.end();}
    
    const_iterator begin() const {return data.begin();}
    const_iterator end()   const {return data.end();}
  
    class Iterator : public ContactMapIterator
    {
      private:
        const ContactMap* cmap;
        const_iterator it;
        bool iterating;
        
        Iterator(const ContactMap* c)
        : cmap(c)
        {
          reset();
        }
      
      public:
        friend class ContactMap;
      
        int getIMin() const { return cmap->getIMin(); }
        int getIMax() const { return cmap->getIMax(); }
//         double getFactor() const { return cmap->getFactor(); }
//         double getDefaultValue() const { return cmap->getDefaultValue(); }
        
        bool next()
        {
          if (!iterating)
            iterating = true;
          else
            it++;
          return it != cmap->end();
        }
        
        std::string getKey() const
        {
          return it->first;
        }
        
        Contact getContact() const
        {
          return it->second;
        }
        
        void reset()
        {
          it = cmap->begin();
          iterating = false;
        }
    };
    
    Iterator getIterator() const
    {
      return Iterator(this);
    }
};



//
// Serialisation & Unserialisation of ContactMap objects
//

void serializeContactMap(const ContactMap& c, std::ostream& out);
void serializeContactMap(const ContactMap& c, const char* filename);

ContactMap unserializeContactMap(const char* filename);


#endif
