/*
 *  scores.h
 *
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#ifndef PROTEINS_H
#define PROTEINS_H

#include <cmath>
#include <vector>
#include "pdb.h"
#include "contactmap.h"


namespace Scores
{
  double distance(const Atom& a, const Atom& b);
  double square_distance(const Atom& a, const Atom& b);
  double RMSD(const Pdb& a, const Pdb& b);
  
  namespace RAPDF
  {
    const unsigned int aaTotalAtoms = 167;
    
    std::vector<double> density(const std::vector<Pdb>&);
    std::vector<double> normaliseDensity(const std::vector<double>& densities, double weightfactor=4.5);
    
    void addCount(const Atom& atm_i, const Atom& atm_j, ContactMap& atom_contact, double increment);
    //void addCount(double dist, std::string key_i, std::string key_j, ContactMap& atom_contact, double increment);
    void addCountConsensus(const Atom& atm_i, const Atom& atm_j, ContactMap& atom_contact, bool normalise);
    
    void buildContactMap(const Pdb& allAtom, ContactMap& atom_contact, char mode, bool useConsensus=false, bool normalise=false);
    void buildContactMap_intra(const Pdb& atoms, ContactMap& atom_contact);
    void buildContactMap_intra_consensus(const Pdb& atoms, ContactMap& atom_contact, bool normalise);
    void buildContactMap_inter(const Pdb& atoms1, const Pdb& atoms2, ContactMap& atom_contact);
    void buildContactMap_inter_consensus(const Pdb& atoms1, const Pdb& atoms2, ContactMap& atom_contact, bool normalise);

//     double score(const ContactMap& myContacts, const ContactMap& db);
//     double score(ContactMapReader& myContacts, const ContactMap& db);
    double score(ContactMapIterator& myContacts, const ContactMap& db, double add=0.0, int normalise=0);
  }
  
  inline double expectedNumberOfContacts(int N, double a=(4.0/3.0)*3.1415926, double b=1.0, double Cres = 5.5, double Fext = 0.38)
  {
    /// This equation estimates the number of contacts in a globular domain, given the domain length (number of residues)
    ///
    /// Reference: Sanne Abeln, 2007, University of Oxford, DPhil thesis, page 131
    return Cres * (N - Fext * (a/b) * ( 3.0 * pow(b*N/a, 2.0/3.0) - 3.0 * pow(b*N/a, 1.0/3.0) + 1 ));
  }
}


#endif
