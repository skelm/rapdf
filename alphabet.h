/*
 *  alphabet.h
 *
 *  Created by Sebastian Kelm on 13/07/2009.
 *
 */

#ifndef ALPHABET_H
#define ALPHABET_H

#include <map>
#include <string>
//#include "skutil.h"
#include "contactmap.h"


namespace Alphabet
{
  typedef std::map<std::string, std::string> ResidueMap;
  
  namespace Mappings
  {
    ResidueMap aa2type;
    aa2type["GLY"] = "NONPOLAR";
    aa2type["ALA"] = "NONPOLAR";
    aa2type["VAL"] = "NONPOLAR";
    aa2type["LEU"] = "NONPOLAR";
    aa2type["ILE"] = "NONPOLAR";
    aa2type["MET"] = "NONPOLAR";
    aa2type["PRO"] = "PRO";
    aa2type["PHE"] = "AROMATIC";
    aa2type["TYR"] = "AROMATIC";
    aa2type["TRP"] = "AROMATIC";
    aa2type["SER"] = "POLAR";
    aa2type["THR"] = "POLAR";
    aa2type["CYS"] = "POLAR";
    aa2type["ASN"] = "POLAR";
    aa2type["GLN"] = "POLAR";
    aa2type["LYS"] = "POSITIVE";
    aa2type["ARG"] = "POSITIVE";
    aa2type["HIS"] = "POSITIVE";
    aa2type["ASP"] = "NEGATIVE";
    aa2type["GLU"] = "NEGATIVE";
    
    aa2type["XLE"] = "NONPOLAR"; // LEU or ILE
    aa2type["SEC"] = "POLAR"; // Selenocysteine
    aa2type["ASX"] = "POLAR"; // ASP or ASN
    aa2type["GLX"] = "POLAR"; // GLU or GLN
    aa2type["XAA"] = "UNKNOWN"; // Unknown amino acid
    aa2type["UNK"] = "UNKNOWN"; // Unknown amino acid
    aa2type["XXX"] = "UNKNOWN"; // Unknown amino acid
  }
  
  void transform(ContactMapIterator& in, ContactMap& out, const ResidueMap& mapping);
  
  ContactMap simplify(ContactMapIterator& in, const ResidueMap& mapping = Mappings::aa2type);
}


#endif
