/*
 *  scores.cpp
 *
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include "skutil.h"
#include "scores.h"

#define P(EX) cout << #EX << ": " << EX << endl;


using namespace std;



double Scores::distance(const Atom& a, const Atom& b)
{
  return sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2));
}

double Scores::square_distance(const Atom& a, const Atom& b)
{
  return pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2);
}

double Scores::RMSD(const Pdb& a, const Pdb& b)
{
  require(a.size() == b.size(), "Scores::RMSD : Structures need to be of the same length");
  require(a.size() > 0, "Scores::RMSD : Structures need to be longer than 0 residues.");
  
  double sumdist = 0.0;
  for (unsigned int i=0; i<a.size(); i++)
  {
    sumdist += Scores::square_distance(a[i], b[i]);
  }
  return sqrt(sumdist / a.size());
}

vector<double> Scores::RAPDF::density(const vector<Pdb>& decoys)
{
  const int n = decoys.size();
  vector<double> scores(n);
  
  // Compare all possible pairs (only once)
  // and add each pair score to both decoys.
  //
  double tempscore = 0.0;
  for (int i=0; i<n-1; i++)
  {
    for (int j=i+1; j<n; j++)
    {
      tempscore = Scores::RMSD(decoys[i], decoys[j]) / n;
      scores[i] += tempscore;
      scores[j] += tempscore;
    }
  }
  
  return scores;
}

vector<double> Scores::RAPDF::normaliseDensity(const vector<double>& densities, double weightfactor)
{
  const int n = densities.size();
  vector<double> scores(n);
//   vector<double> weights(n);
  double sumOfWeights = 0.0;
  
  for (int i=0; i<n; i++)
  {
    double w = exp(-1 * weightfactor * densities[i]);
    sumOfWeights += w;
    scores[i] = w * densities[i];
  }
  
  for (int i=0; i<n; i++)
  {
    scores[i] /= sumOfWeights;
  }
  
  return scores;
}


void Scores::RAPDF::buildContactMap(const Pdb& atoms, ContactMap& atom_contact, char mode, bool useConsensus, bool normalise)
{
  switch(mode)
  {
    case 'a': // all vs all: any contacts within the structure
    {
      if (useConsensus)
          buildContactMap_intra_consensus(atoms, atom_contact, normalise);
        else
          buildContactMap_intra(atoms, atom_contact);
      break;
    }
    case 'c': // intra-chain contacts only
    {
      vector<Pdb> chains;
      vector<string> chainids;
      atoms.splitChains(chains, chainids);
      cerr << "Scores::RAPDF::buildContactMap : found chains : " << toString(chainids) << "\n";
      for (unsigned int i=0; i<chains.size(); i++)
        if (useConsensus)
          buildContactMap_intra_consensus(chains[i], atom_contact, normalise);
        else
          buildContactMap_intra(chains[i], atom_contact);
      break;
    }
    case 'd': // docking mode: inter-chain contacts only
    {
      vector<Pdb> chains;
      vector<string> chainids;
      atoms.splitChains(chains, chainids);
      cerr << "Scores::RAPDF::buildContactMap : found chains : " << toString(chainids) << "\n";
      for (unsigned int i=1; i<chains.size(); i++)
        for (unsigned int j=0; j<i; j++)
          if (useConsensus)
            buildContactMap_inter_consensus(chains[i], chains[j], atom_contact, normalise);
          else
            buildContactMap_inter(chains[i], chains[j], atom_contact);
      break;
    }
    default:
    {
      throw ValueError(string("Scores::RAPDF::buildContactMap : Only modes 'a' 'c' and 'd' are allowed. Given: ")+mode);
    }
  }
}


inline void Scores::RAPDF::addCount(const Atom& atm_i, const Atom& atm_j, ContactMap& atom_contact, double increment)
{
      double dist = distance(atm_i, atm_j);

      if (dist > atom_contact.getIMax())
        return;

      string key_i = atm_i.res + ' ' + atm_i.atom;
      string key_j = atm_j.res + ' ' + atm_j.atom;

      string key = (key_i < key_j) ? (key_i + ' ' + key_j) : (key_j + ' ' + key_i);
//       cerr << key << " : " << dist << "\n";
      atom_contact.add(key, dist, increment);
}


inline void Scores::RAPDF::addCountConsensus(const Atom& atm_i, const Atom& atm_j, ContactMap& atom_contact, bool normalise)
{
      double dist = distance(atm_i, atm_j);
      
      if (dist > atom_contact.getMaxCutoff())
        return;
      
      double quotient = normalise ? (atm_i.consensus.totalCount * atm_j.consensus.totalCount) : 1.0;
      
      for (unsigned int k=0; k<atm_i.consensus.residues.size(); k++)
      {
        string key_i = atm_i.consensus.residues[k] + ' ' + atm_i.atom;
        for (unsigned int l=0; l<atm_j.consensus.residues.size(); l++)
        {
          string key_j = atm_j.consensus.residues[l] + ' ' + atm_j.atom;
          string key = (key_i < key_j) ? (key_i + ' ' + key_j) : (key_j + ' ' + key_i);
          
          //cerr << "Contact : " << key << "\t" << dist << "\t" << atm_i.consensus.counts[k] * atm_j.consensus.counts[l] << " / " << quotient << "\n";
          atom_contact.add(key, dist,
              atm_i.consensus.counts[k] * atm_j.consensus.counts[l] / quotient );
        }
      }
}



void Scores::RAPDF::buildContactMap_intra(const Pdb& atoms, ContactMap& atom_contact)
{ 
  const int nAll = atoms.size();
//   atom_contact.setFactor(factor);

  for(int i=0; i<nAll-1; i++)
  { 
    for(int j=i+1; j<nAll; j++)
    {
      if (atoms[i].ires == atoms[j].ires and atoms[i].chain == atoms[j].chain and atoms[i].inscode == atoms[j].inscode)
        continue; // skip comparisons within same amino acid
      
      addCount(atoms[i], atoms[j], atom_contact, 1.0);
    }
  }
}


void Scores::RAPDF::buildContactMap_intra_consensus(const Pdb& atoms, ContactMap& atom_contact, bool normalise)
{ 
  const int nAll = atoms.size();
//   atom_contact.setFactor(factor);

  for(int i=0; i<nAll-1; i++)
  { 
    for(int j=i+1; j<nAll; j++)
    {
      if (atoms[i].ires == atoms[j].ires and atoms[i].chain == atoms[j].chain and atoms[i].inscode == atoms[j].inscode)
        continue; // skip comparisons within same amino acid

      addCountConsensus(atoms[i], atoms[j], atom_contact, normalise);
    }
  }
}



void Scores::RAPDF::buildContactMap_inter(const Pdb& atoms1, const Pdb& atoms2, ContactMap& atom_contact)
{ 
  const int n1 = atoms1.size();
  const int n2 = atoms2.size();

//   atom_contact.setFactor(factor);

  for(int i=0; i<n1; i++)
  { 
    for(int j=0; j<n2; j++)
    {
      addCount(atoms1[i], atoms2[j], atom_contact, 1.0);
    }
  }
}



void Scores::RAPDF::buildContactMap_inter_consensus(const Pdb& atoms1, const Pdb& atoms2, ContactMap& atom_contact, bool normalise)
{ 
  const int n1 = atoms1.size();
  const int n2 = atoms2.size();

//   atom_contact.setFactor(factor);

  for(int i=0; i<n1; i++)
  { 
    for(int j=0; j<n2; j++)
    {
      addCountConsensus(atoms1[i], atoms2[j], atom_contact, normalise);
    }
  }
}




double Scores::RAPDF::score(ContactMapIterator& myContacts, const ContactMap& db, double add, int normalise)
{
  const int imin = db.getMinCutoff();
  const int imax = db.getMaxCutoff();
  //const double weight = myContacts.getFactor();
  
  vector<double> columnTotals = ((ContactMap)db).getColumnSums();
  
  if (add != 0)
  {
    const int dbSize = max(db.size(), aaTotalAtoms);
    for(unsigned i=0; i<columnTotals.size(); i++)
      columnTotals[i] += add * dbSize;
//       columnTotals[i] += add * db.size();
  }
  
#ifdef DEBUG
   cout << "sums of columns: " << toString(columnTotals) << "\n";
#endif
  
  double allSum = sum(columnTotals);
  
#ifdef DEBUG
  cout << "sum of all: " << allSum << "\n";
#endif
  
  // Convert columnTotals to fractions
  for(unsigned int i=0; i<columnTotals.size(); i++)
    columnTotals[i] /= allSum;
  
#ifdef DEBUG
  cout << "Fractions of column totals: " << toString(columnTotals) << "\n";
#endif
  
  // columnTotals now represents the probabilities of observing each distance (in integer steps) from minCutoff to maxCutoff
  
  
  double score = 0.0; // this will hold the end result
  
  Contact dbval(db.getIMin(), db.getIMax(), 0.0); // buffer for the values of the current Contact object in the database
  
  int missingEntries = 0;
  double decoyTotalContacts = 0.0;
  double decoyScoredContacts = 0.0;
  
  while(myContacts.next())
  {
    const string  key    = myContacts.getKey();
    const Contact val    = myContacts.getContact();
    
    if (normalise == -2)
      decoyTotalContacts += sum(val);
    
    try
    {
      dbval = db[key];
      #ifdef DEBUG
      cout << key << "\t" << toString(db[key].asVector()) << "\n";
      #endif
    }
    catch (NotFound e)
    {
      missingEntries++;
      continue;
      
//       if (add <= 0)
//         continue;
//       
//       dbval.fill(0.0);
//       
//       #ifdef DEBUG
//       for (int dist=imin; dist<=imax; dist++)
//       {
//         require(dbval[dist] == 0.0, "Filling dbval with all 0.0 values failed!");
//       }
//       #endif
    }
    
//     require(dbval.asVector() == db[key].asVector(), "BUG! Assignment failed... dbval != db[key]");
    
    double sumDbVal = sum(dbval) + (add * dbval.size());
    
    for (int dist=imin; dist<=imax; dist++)
    {
        if (columnTotals[dist-imin] <= 0.0 or (add + dbval[dist]) <= 0.0)
          continue;
        
        double valDist = val[dist];
        
        #ifdef DEBUG
        cout << "dist=" << dist << "\t" << score << " += " << (valDist) << " * -1 * log( (" << add << " + " << dbval[dist] << ") / " << sumDbVal << " / " << columnTotals[dist-imin] << " );\n";
        #endif
        
        score += valDist * -1.0 * log( (add + dbval[dist]) / sumDbVal / columnTotals[dist-imin] );
        decoyScoredContacts += valDist;
    }
  }
  
  #ifdef DEBUG
  cout << "score:   " << score << "\n";
  cerr << "missing entries: " << missingEntries << "\n";
  #endif
  
  
  if (normalise > 0)
    score /= expectedNumberOfContacts(normalise);
  else if (normalise == -1)
    score /= decoyScoredContacts;
  else if (normalise == -2)
    score /= decoyTotalContacts;
  
  return score;
}
