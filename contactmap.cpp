/*
 *  contactmap.cpp
 *
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>

#include "skutil.h"
#include "contactmap.h"

#define P(EX) cout << #EX << ": " << EX << endl;


using namespace std;


Contact::Contact(int imn, int imx, double defValue)
: data(imx-imn+1, defValue),
  imin(imn),
  imax(imx)
{
  require(imn>=0 and imx>imn, "Contact::Contact : imin not <= 0 or imax <= imin");
}

void Contact::fill(double value)
{
  for (unsigned int i=0; i<data.size(); i++)
    data[i] = value;
}

void Contact::add(Contact& other, double factor)
{
  require((other.getIMin() == imin) and (other.getIMax() == imax), "Contact::add : contact min and max values do not match!");
  
  for (int i=imin; i<=imax; i++)
    (*this)[i] += other[i] * factor;
}

double& Contact::operator[](int i)
{
  if (!(i>=0 and i<=imax))
  {
    throw OutOfBounds(string("Contact::operator[](int) : illegal index: ") + toString(i));
  }
  
  if (i < imin)
    i = imin;
  
  return data[i-imin];
}
double& Contact::operator[](double i)
{
  if (!(i>=0.0 and i<=imax))
  {
    throw OutOfBounds(string("Contact::operator[](double) : illegal index: ") + toString(i));
  }
  
  int ii = (int)ceil(i)-imin;
  if (ii < 0)
    ii = 0;
  
  return data[ii];
}
const double& Contact::operator[](int i) const
{
  if (!(i>=0 and i<=imax))
  {
    throw OutOfBounds(string("Contact::operator[](int) : illegal index: ") + toString(i));
  }
  
  if (i < imin)
    i = imin;
  
  return data[i-imin];
}
const double& Contact::operator[](double i) const
{
  if (!(i>=0.0 and i<=imax))
  {
    throw OutOfBounds(string("Contact::operator[](double) : illegal index: ") + toString(i));
  }
  
  int ii = (int)ceil(i)-imin;
  if (ii < 0)
    ii = 0;
  
  return data[ii];
}



void Contact::print(ostream& out) const
{
  for(int i=imin; i<=imax; i++)
  {
    out << "\t" << i << ", " << (*this)[i] << "\n";
  }
}







ContactMapReader::ContactMapReader(const char* filename)
    : fname(filename),
      fin(filename, std::ios::binary),
      imin(0), imax(0) /*, factor(1), defaultValue(0) */
{
  init();
}

ContactMapReader::ContactMapReader(const ContactMapReader& cmr)
    : fname(cmr.fname),
      fin(cmr.fname, std::ios::binary),
      imin(0), imax(0) /*, factor(1), defaultValue(0) */
{
  init();
}

void ContactMapReader::init()
{
  require(fin, string("ContactMapReader : Could not read input file: ") + fname + "\n");
  
  std::string line;
  getline(fin, line);
  std::istringstream tokens(line);
  std::string token;
  
//   cout << "ContactMapReader : \n";
//   cout << line << "\n";
  
  if (!tokens.good())
    throw ParsingError("ContactMapReader::init() : Cannot parse first line of file.");
  
  tokens >> imin;
  if (tokens.fail())
    throw ParsingError("ContactMapReader::init() : Error parsing parameter 'imin'.");
  
  tokens >> imax;
  if (tokens.fail())
    throw ParsingError("ContactMapReader::init() : Error parsing parameter 'imax'.");
  
//   tokens >> factor;
//   if (tokens.fail())
//   {
//     cerr << "ContactMapReader::init() : Error parsing parameters 'factor', 'defaultValue'. Falling back to defaults ("<< factor <<", "<< defaultValue <<").\n";
//   }
//   else
//   {
//     tokens >> defaultValue;
//     if (tokens.fail())
//       cerr << "ContactMapReader::init() : Error parsing parameter 'defaultValue'. Falling back to default ("<< defaultValue <<").\n";
//   }
  
  c = Contact(imin, imax, 0.0);
        
  startpos = fin.tellg();
}

bool ContactMapReader::next()
{ 
  std::string line;

  if(!getline(fin, line))
    return false;
  
  key = line;
  
  if(!getline(fin, line))
    return false;
  
  std::istringstream tokens(line);
  std::string token;
  
  int i=imin;
  while(tokens >> token)
  {
    if (i > imax)
      throw OutOfBounds("unserializeContactMap::next : too many values for given boundaries (imin, imax)");
    double val = convertTo<double>(token);
    c[i++] = val;
  }
  
  return true;
}

std::string ContactMapReader::getKey() const
{
  return key;
}

Contact ContactMapReader::getContact() const
{
  return c;
}

void ContactMapReader::reset()
{
  fin.clear();
  fin.seekg(startpos);
//   fin.close();
//   fin.open(fname);
//   string line;
//   getline(fin, line);
}

void ContactMapReader::close()
{
  fin.close();
}







ContactMap::ContactMap(int imin, int imax, double fact, double defVal)
: minCutoff(imin),
  maxCutoff(imax),
//   factor(fact),
  columnSums(imax-imin+1, 0.0),
  redoSums(true)
//   defaultValue(defVal)
{
}
ContactMap::ContactMap(const ContactMap& c)
: data(c.data),
  minCutoff(c.minCutoff),
  maxCutoff(c.maxCutoff),
//   factor(c.factor),
  columnSums(c.columnSums),
  redoSums(c.redoSums)
//   defaultValue(c.defaultValue)
{
}
ContactMap::ContactMap(ContactMapIterator& cread)
: minCutoff(cread.getIMin()),
  maxCutoff(cread.getIMax()),
//   factor(cread.getFactor()),
  columnSums(cread.getIMax()-cread.getIMin()+1, 0.0),
  redoSums(true)
//   defaultValue(cread.getDefaultValue())
{
  cread.reset();
  while(cread.next())
  {
    string key = cread.getKey();
    Contact  c = cread.getContact();
    //add(key, c, 1.0); // BUG?
    (*this)[key] = c;
  }
}


bool ContactMap::hasContacts(std::string key) const
{
  const_iterator iter = data.find(key);
  return iter != data.end();
}

Contact& ContactMap::initialiseContacts(const std::string key)
{
  redoSums = true;
  return data[key] = Contact(minCutoff, maxCutoff, 0.0);
}

void ContactMap::add(const string key, double dist, double increment)
{
  require(dist>=0, "ContactMap::add : Distance < 0!");
  if (dist>maxCutoff)
    throw OutOfBounds("ContactMap::add() : index="+toString(dist));
  
  (*this)[key][dist] += increment;
  redoSums = true;
}

void ContactMap::add(const string key, Contact contact, double factor)
{
  (*this)[key].add(contact, factor);
  redoSums = true;
}

void ContactMap::add(ContactMap& contacts, double factor)
{
//   const double factor = contacts.getFactor();
  for (iterator iter = contacts.data.begin(); iter != contacts.data.end(); ++iter)
  {
    (*this)[iter->first].add(iter->second, factor);
  }
  redoSums = true;
}

double ContactMap::getTotalSum()
{
  return sum(getColumnSums());
}

vector<double> ContactMap::getColumnSums()
{
  if (redoSums)
  {
    for(const_iterator iter = data.begin(); iter != data.end(); ++iter)
    {
      //string  key = iter->first;
      Contact val = iter->second;
      
      unsigned int i=0;
      for (Contact::const_iterator it=val.begin(); it<val.end(); it++)
      {
        //cout << "ContactMap::getColumnSums() : " << columnSums[i] << " += " << *it << "\n";
        columnSums[i++] += *it;
      }
    }
#ifdef DEBUG
     cout << "ContactMap::getColumnSums() : " << toString(columnSums) << "\n";
#endif
    redoSums = false;
  }
  return columnSums;
}

void ContactMap::print(ostream& out) const
{
  for(const_iterator iter = data.begin(); iter != data.end(); ++iter)
  {
    string  key = iter->first;
    Contact val = iter->second;
    
    out << "Key: " << key << "\n";
    out << "Value (counts): \n";
    val.print(out);
    out << "\n";
  }
}


Contact& ContactMap::operator[](std::string key)
{
  if (!hasContacts(key))
  {
    return initialiseContacts(key);
//     throw NotFound(string("ContactMap::operator[] : Key not found : ") + key);
  }
  return data[key];
}

const Contact& ContactMap::operator[](std::string key) const
{
  const_iterator it = data.find(key);
  if (it != data.end())
    return it->second;
  else
    throw NotFound(string("ContactMap::operator[] : Key not found : ") + key);
}






void serializeContactMap(const ContactMap& c, std::ostream& out)
{
    const int imin = c.getMinCutoff(); // smallest integer distance
    const int imax = c.getMaxCutoff(); // largest integer distance
    
    out << imin << " " << imax /* << " " << c.getFactor() << " " << c.getDefaultValue() */ << "\n";
    for(ContactMap::const_iterator it = c.begin(); it != c.end(); ++it)
    {
      // Key (e.g. "ARG C VAL C")
      out << it->first << "\n";
      
      // Values (counts for each distance bin, space-separated)
            Contact::const_iterator itv    = it->second.begin();
      const Contact::const_iterator itvend = it->second.end();
      for (; itv<itvend; itv++)
      {
        out << " " << *itv;
      }
      out << "\n";
    }
}
void serializeContactMap(const ContactMap& c, const char* filename)
{
    ofstream out(filename);
    require(out, string("Could not open output file : ")+string(filename));
    serializeContactMap(c, out);
    out.close();
}


ContactMap unserializeContactMap(const char* filename)
{
  ContactMapReader cread = ContactMapReader(filename);
  ContactMap cmap(cread.getIMin(), cread.getIMax() /*, cread.getFactor(), cread.getDefaultValue() */);
  
//   cout << "Default value: " << cmap.getDefaultValue() << "\n";
  
  while(cread.next())
  {
    string key = cread.getKey();
    Contact c  = cread.getContact();
//     cout << key << "\t" << toString(c) << "\n";
//     cout << key << "\t" << toString(cmap[key]) << "\n";
    cmap.add(key, c, 1.0);
//     cout << key << "\t" << toString(cmap[key]) << "\n";
  }
//   exit(1);
  return cmap;
}
